//
//  MenuViewController.swift
//  SendBird-iOS
//
//  Created by Jed Kyung on 10/13/16.
//  Copyright © 2016 SendBird. All rights reserved.
//

import UIKit
import SendBirdSDK

class MenuViewController: UIViewController, SBDConnectionDelegate {
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var openChannelView: UIView!
    @IBOutlet weak var groupChannelView: UIView!
    @IBOutlet weak var privateChannelView: UIView!
    
    var groupChannelListViewController: GroupChannelListViewController?
    var mode = "none"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let negativeRightSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeRightSpacer.width = -2
        let rightDisconnectItem = UIBarButtonItem(title: Bundle.sbLocalizedStringForKey(key: "DisconnectButton"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(disconnect))
        rightDisconnectItem.setTitleTextAttributes([NSAttributedStringKey.font: Constants.navigationBarButtonItemFont()], for: UIControlState.normal)
        
        let negativeLeftSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeLeftSpacer.width = -2
        let leftProfileItem = UIBarButtonItem(title: Bundle.sbLocalizedStringForKey(key: "Profile"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(profile))
        leftProfileItem.setTitleTextAttributes([NSAttributedStringKey.font: Constants.navigationBarButtonItemFont()], for: UIControlState.normal)
        
        self.navItem.rightBarButtonItems = [negativeRightSpacer, rightDisconnectItem]
        self.navItem.leftBarButtonItems = [negativeLeftSpacer, leftProfileItem]
        
        SBDMain.add(self as SBDConnectionDelegate, identifier: self.description)
        
        if (UIApplication.shared.delegate as! AppDelegate).receivedPushChannelUrl != nil {
            let channelUrl = (UIApplication.shared.delegate as! AppDelegate).receivedPushChannelUrl
            if channelUrl != nil {
                SBDGroupChannel.getWithUrl(channelUrl!, completionHandler: { (channel, error) in
                    let vc = GroupChannelChattingViewController()
                    vc.groupChannel = channel
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func pressPrivateChannelButton(_ sender: AnyObject) {
        self.openChannelView.backgroundColor = UIColor.white
        self.groupChannelView.backgroundColor = UIColor.white
        self.privateChannelView.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
    }

    @IBAction func clickPrivateChannelButton(_ sender: AnyObject) {
        self.openChannelView.backgroundColor = UIColor.white
        self.groupChannelView.backgroundColor = UIColor.white
        self.privateChannelView.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
        
        self.showGroupChannelList()
        mode = "private"
    }
    
    @IBAction func pressOpenChannelButton(_ sender: AnyObject) {
        self.openChannelView.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
        self.groupChannelView.backgroundColor = UIColor.white
        self.privateChannelView.backgroundColor = UIColor.white
    }
    
    @IBAction func clickOpenChannelButton(_ sender: AnyObject) {
        self.openChannelView.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
        self.groupChannelView.backgroundColor = UIColor.white
        self.privateChannelView.backgroundColor = UIColor.white

        let vc = OpenChannelListViewController(nibName: "OpenChannelListViewController", bundle: Bundle.main)
        self.present(vc, animated: true) {
            vc.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        mode = "public"
    }
    
    @IBAction func pressGroupChannelButton(_ sender: AnyObject) {
        self.openChannelView.backgroundColor = UIColor.white
        self.groupChannelView.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
        self.privateChannelView.backgroundColor = UIColor.white
    }
    
    private func showGroupChannelList() {
        if self.groupChannelListViewController == nil {
            self.groupChannelListViewController = GroupChannelListViewController(nibName: "GroupChannelListViewController", bundle: Bundle.main)
            self.groupChannelListViewController?.addDelegates()
        }
        self.present(self.groupChannelListViewController!, animated: true) {
            self.groupChannelListViewController?.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    @IBAction func clickGroupChannelButton(_ sender: AnyObject) {
        self.openChannelView.backgroundColor = UIColor.white
        self.groupChannelView.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1)
        self.privateChannelView.backgroundColor = UIColor.white
        self.showGroupChannelList()
        mode = "group"
    }

    @objc func disconnect() {
        SBDMain.unregisterAllPushToken { (response, error) in
            if error != nil {
                print("Unregister all push tokens. Error: %@", error!)
            }
            
            SBDMain.disconnect(completionHandler: {
                DispatchQueue.main.async {
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    self.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
    
    @objc private func profile() {
        let vc = UserProfileViewController()
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    // MARK: GroupChannelChattingViewController
    func didStartReconnection() {
        
    }
    
    func didSucceedReconnection() {
        if (UIApplication.shared.delegate as! AppDelegate).receivedPushChannelUrl != nil {
            let channelUrl = (UIApplication.shared.delegate as! AppDelegate).receivedPushChannelUrl
            
            var topViewController = UIApplication.shared.keyWindow?.rootViewController
            while ((topViewController?.presentedViewController) != nil) {
                topViewController = topViewController?.presentedViewController
            }
            
            if topViewController is GroupChannelChattingViewController {
                if (topViewController as! GroupChannelChattingViewController).groupChannel.channelUrl == channelUrl {
                    return
                }
            }
            
            ((UIApplication.shared).delegate as! AppDelegate).receivedPushChannelUrl = nil
            SBDGroupChannel.getWithUrl(channelUrl!, completionHandler: { (channel, error) in
                let vc = GroupChannelChattingViewController()
                vc.groupChannel = channel
                DispatchQueue.main.async {
                    topViewController?.present(vc, animated: true, completion: nil)
                }
            })
        }
    }
    
    func didFailReconnection() {
        
    }
}
