//
//  ViewController.swift
//  SendBird-iOS
//
//  Created by Jed Kyung on 10/6/16.
//  Copyright © 2016 SendBird. All rights reserved.
//

import UIKit
import SendBirdSDK
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit

class ViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tableViewMain: UITableView!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var userIdTextField: UITextField!
    @IBOutlet weak var userIdLineView: UIView!
    @IBOutlet weak var nicknameTextField: UITextField!
    @IBOutlet weak var nicknameLineView: UIView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBOutlet weak var userIdLabelBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var nicknameLabelBottomMargin: NSLayoutConstraint!

    @IBOutlet weak var loginFacebookButton: UIButton!
    @IBOutlet weak var connectWithFacebookButton: FBSDKLoginButton!

    var currentUserID = "username"
    var currentNickname = "nickname"
    var currentToken = "AABB4433DD"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // resign responder (dismiss keyboards)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        // Version
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")
        if path != nil {
            let infoDict = NSDictionary(contentsOfFile: path!)
            let sampleUIVersion = infoDict?["CFBundleShortVersionString"] as! String
            let version = String(format: "Sample UI v%@ / SDK v%@", sampleUIVersion, SBDMain.getSDKVersion())
            self.versionLabel.text = version
        }
        
        self.userIdTextField.delegate = self
        self.nicknameTextField.delegate = self
        
        self.userIdLabel.alpha = 0
        self.nicknameLabel.alpha = 0
        
        let userId = UserDefaults.standard.object(forKey: "sendbird_user_id") as? String
        let userNickname = UserDefaults.standard.object(forKey: "sendbird_user_nickname") as? String
        
//        self.userIdLineView.backgroundColor = Constants.textFieldLineColorNormal()
//        self.nicknameLineView.backgroundColor = Constants.textFieldLineColorNormal()
        
        self.userIdTextField.text = userId
        self.nicknameTextField.text = userNickname
        
//        connectWithFacebookButton.readPermissions = ["public_profile", "email"]
        
//        self.connectButton.setBackgroundImage(Utils.imageFromColor(color: Constants.connectButtonColor()), for: UIControlState.normal)
        
        self.indicatorView.hidesWhenStopped = true
        
        if userId != nil && (userId?.count)! > 0 && userNickname != nil && (userNickname?.count)! > 0 {
            self.connect()
        }
    }

    @IBAction func clickConnectButton(_ sender: AnyObject) {
        self.connect()
    }
    
    @IBAction func onTapLoginWithFacebook() {
        FBSDKLoginManager().logIn(withReadPermissions: ["email","public_profile"], from: self) { (result, error) in
            if (error != nil)   {
                print("Login fail")
                return
            } else if (result?.isCancelled == true){
                print("ON CANCEL")
            } else {
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                print("Token: \(credential)")
                print("Token: \(FBSDKAccessToken.current().tokenString!)")
//                Auth.auth().signIn(with: credential) { (user, error) in
//                    if (error != nil){
//                        // ...
//                        return
//                    }else {
//                        self.showLoading()
//                        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
//                        DispatchQueue.main.asyncAfter(deadline: when) {
//                            self.routeToHomeView(animation: true)
//                            self.hideLoading()
//                        }
//                    }
//                }
            }
            
        }
//        connectWithFacebookButton.sendActions(for: .touchUpInside)
//        loginFacebookButton.on
    }
    
    func connect() {
        let trimmedUserId: String = (self.userIdTextField.text?.trimmingCharacters(in: NSCharacterSet.whitespaces))!
        let trimmedNickname: String = (self.nicknameTextField.text?.trimmingCharacters(in: NSCharacterSet.whitespaces))!
        if trimmedUserId.count > 0 && trimmedNickname.count > 0 {
            self.userIdTextField.isEnabled = false
            self.nicknameTextField.isEnabled = false
            
            self.indicatorView.startAnimating()
            
            SBDMain.connect(withUserId: trimmedUserId, completionHandler: { (user, error) in
                if error != nil {
                    DispatchQueue.main.async {
                        self.userIdTextField.isEnabled = true
                        self.nicknameTextField.isEnabled = true
                        
                        self.indicatorView.stopAnimating()
                    }
                    
                    let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                    let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                    vc.addAction(closeAction)
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                if SBDMain.getPendingPushToken() != nil {
                    SBDMain.registerDevicePushToken(SBDMain.getPendingPushToken()!, unique: true, completionHandler: { (status, error) in
                        if error == nil {
                            if status == SBDPushTokenRegistrationStatus.pending {
                                print("Push registeration is pending.")
                            }
                            else {
                                print("APNS Token is registered.")
                            }
                        }
                        else {
                            print("APNS registration failed.")
                        }
                    })
                }
                
                SBDMain.updateCurrentUserInfo(withNickname: trimmedNickname, profileUrl: nil, completionHandler: { (error) in
                    DispatchQueue.main.async {
                        self.userIdTextField.isEnabled = true
                        self.nicknameTextField.isEnabled = true
                        
                        self.indicatorView.stopAnimating()
                    }
                    
                    if error != nil {
                        let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
                        let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
                        vc.addAction(closeAction)
                        DispatchQueue.main.async {
                            self.present(vc, animated: true, completion: nil)
                        }
                        
                        SBDMain.disconnect(completionHandler: {
                            
                        })
                        
                        return
                    }
                    
                    UserDefaults.standard.set(SBDMain.getCurrentUser()?.userId, forKey: "sendbird_user_id")
                    UserDefaults.standard.set(SBDMain.getCurrentUser()?.nickname, forKey: "sendbird_user_nickname")
                })
                
                DispatchQueue.main.async {
                    let vc = MenuViewController(nibName: "MenuViewController", bundle: Bundle.main)
                    self.present(vc, animated: true, completion: nil)
                }
            })
        }
    }

    override func viewWillAppear(_ animated: Bool) {

//        if FBSDKAccessToken.current() != nil {
//            // optional blinding
//            if let facebookUserID = FBSDKAccessToken.current().userID {
//                // User is logged in, do work such as go to next view controller.
//                currentUserID = facebookUserID
//                print("ได้ userID: \(currentUserID)")
//            }
//
//            if FBSDKAccessToken.current().tokenString != nil {
//                currentToken = FBSDKAccessToken.current().tokenString!
//                print("ได้ token: \(currentToken)")
//            }
//
//
//        } else {
//            print("There's no FB userID")
//        }
    
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.userIdTextField {
            nicknameTextField.becomeFirstResponder()
        } else if textField == self.nicknameTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    //This function is called when the tap is recognized.
    @objc func dismissKeyboard() {
        // Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK: UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == self.userIdTextField {
//            self.userIdLineView.backgroundColor = Constants.textFieldLineColorSelected()
//        } else if textField == self.nicknameTextField {
//            self.nicknameLineView.backgroundColor = Constants.textFieldLineColorSelected()
//        }
        tableViewMain.isScrollEnabled = true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == self.userIdTextField {
//            self.userIdLineView.backgroundColor = Constants.textFieldLineColorNormal()
//        } else if textField == self.nicknameTextField {
//            self.nicknameLineView.backgroundColor = Constants.textFieldLineColorNormal()
//        }
        tableViewMain.isScrollEnabled = false

    }
}
